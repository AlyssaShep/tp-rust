// to run with the command:
// cargo run haine texts_tests/poem.txt

#![allow(non_snake_case)]

use std::string::String;

use grep_application::Config;
use std::env;
use std::process;

mod lib_json;

fn main() {
    // grep with a poem
    let args: Vec<String> = env::args().collect();

    let config = Config::new(&args).unwrap_or_else(|err| {
        eprintln!(
            "Un problème est survenu lors du traitement des arguments : {}",
            err
        );

        process::exit(1);
    });

    if let Err(e) = grep_application::run(config) {
        eprintln!("Erreur de l'application : {}", e);

        process::exit(1);
    }

    println!("{}", '\n');

    // reading a json file
    let filename: String = "texts_tests/blockchain.json".to_string();
    lib_json::reading(filename);

    // grep with a json file
    let rsearch: String =
        "b6f6991d03df0e2e04dafffcd6bc418aac66049e2cd74b80f14ac86db1e3f0da".to_string();
    let filename: String = "texts_tests/blockchain.json".to_string();
    lib_json::research(rsearch, filename);
}
