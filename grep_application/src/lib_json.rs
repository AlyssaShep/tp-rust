extern crate serde;
extern crate serde_derive;
extern crate serde_json;

use serde_derive::Deserialize;
use serde_derive::Serialize;
use serde_json::Value as JsonValue;

use std::fs;
use std::string::String;

#[derive(Serialize, Deserialize)]
pub struct PrevOut {
    pub hash: String,
    pub value: String,
    pub tx_index: String,
    pub n: String,
}

#[derive(Serialize, Deserialize)]
pub struct Input {
    pub prev_out: PrevOut,
    pub script: String,
}

#[derive(Serialize, Deserialize)]
pub struct Out {
    pub value: String,
    pub hash: String,
    pub script: String,
}

#[derive(Serialize, Deserialize)]
pub struct Blockchain {
    pub hash: String,
    pub inputs: Vec<Input>,
    pub out: Vec<Out>,
}

pub fn reading(filename: String) {
    let content = fs::read_to_string(filename).expect("pb file");

    let lecture = serde_json::from_str(&content);

    if lecture.is_ok() {
        let display1: Vec<JsonValue> = lecture.unwrap();
        println!(
            "hash (JsonValue) : {}",
            display1[0]["hash"].as_str().unwrap()
        );
        println!("{}", '\n');

        let display2: Vec<Blockchain> = serde_json::from_str(&content).unwrap();
        for b in display2.iter() {
            println!("hash     : {}", b.hash);
            for ins in b.inputs.iter() {
                println!("script   : {}", ins.script);
                println!("hash in  : {}", ins.prev_out.hash);
                println!("value    : {}", ins.prev_out.value);
                println!("tx index : {}", ins.prev_out.tx_index);
                println!("n        : {}", ins.prev_out.n);
            }
            for out in b.out.iter() {
                println!("hash out : {}", out.hash);
                println!("value    : {}", out.value);
                println!("script   : {}", out.script);
            }
            println!("{}", '\n');
        }
    } else {
        println!("pas d affichage");
    }
}

// research inside Structure (parsed with struct)
pub fn research(research: String, filename: String) {
    let content = fs::read_to_string(filename).expect("pb file");
    let display2: Vec<Blockchain> = serde_json::from_str(&content).unwrap();

    for b in display2.iter() {
        if b.hash == research {
            println!("hash (in blockchain's struct) : {}", b.hash);
        } else {
            for ins in b.inputs.iter() {
                if ins.script == research {
                    println!("script (in Input's struct)     : {}", ins.script);
                } else if ins.prev_out.hash == research {
                    println!("hash in (in PrevOut's struct)  : {}", ins.prev_out.hash);
                } else if ins.prev_out.value == research {
                    println!("value (in PrevOut's struct)    : {}", ins.prev_out.value);
                } else if ins.prev_out.tx_index == research {
                    println!("tx index (in PrevOut's struct) : {}", ins.prev_out.tx_index);
                } else if ins.prev_out.n == research {
                    println!("n (in PrevOut's struct)        : {}", ins.prev_out.n);
                }
            }
            for out in b.out.iter() {
                if out.hash == research {
                    println!("hash (in Out's struct)   : {}", out.hash);
                } else if out.value == research {
                    println!("value (in Out's struct)  : {}", out.value);
                } else if out.script == research {
                    println!("script (in Out's struct) : {}", out.script);
                }
            }
            println!("{}", '\n');
        }
    }
}
