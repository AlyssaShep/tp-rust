#![allow(non_snake_case)]

use std::env;
use std::error::Error;
use std::fs;

pub struct Config {
    pub query: std::string::String,
    pub filename: std::string::String,
    pub caseSensible: bool,
}

impl Config {
    pub fn new(args: &[String]) -> Result<Config, &str> {
        if args.len() < 3 {
            return Err("not enough args");
        }

        let query = args[1].clone();
        let filename = args[2].clone();

        let caseSensible = env::var("CASE_INSENSITIVE").is_err();

        return Ok(Config {
            query,
            filename,
            caseSensible,
        });
    }
}

pub fn run(config: Config) -> Result<(), Box<dyn Error>> {
    let content = fs::read_to_string(config.filename)?;

    let results = if config.caseSensible {
        recherche(&config.query, &content)
    } else {
        rechercheCasseInsensible(&config.query, &content)
    };

    for line in results {
        println!("{}", line);
    }

    return Ok(());
}

pub fn recherche<'a>(query: &str, content: &'a str) -> Vec<&'a str> {
    let mut resultats = Vec::new();

    for line in content.lines() {
        if line.contains(query) {
            resultats.push(line);
        }
    }

    return resultats;
}

pub fn rechercheCasseInsensible<'a>(query: &str, content: &'a str) -> Vec<&'a str> {
    let query = query.to_lowercase();
    let mut resultats = Vec::new();

    for line in content.lines() {
        if line.to_lowercase().contains(&query) {
            resultats.push(line);
        }
    }

    return resultats;
}
