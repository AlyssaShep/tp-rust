use crate::{Error, MAX_REGISTER_INDEX};

#[allow(dead_code)]
#[derive(Clone, Debug, PartialEq)]
pub enum OpCode {
    LD = 0x00,
    ST = 0x01,
    ADD = 0x02,
    XOR = 0x03,
}

impl OpCode {
    /// Convert a u8 into an OpCode according to the specifications of our architecture
    pub fn from_u8(opcode: u8) -> Result<OpCode, Error> {
        match opcode {
            0x00 => Ok(OpCode::LD),
            0x01 => Ok(OpCode::ST),
            0x02 => Ok(OpCode::ADD),
            0x03 => Ok(OpCode::XOR),
            _ => Err(Error::InvalidOpCode(opcode)),
        }
    }
}

/// A decoded instruction
#[derive(Debug)]
pub struct Instruction {
    opcode: OpCode,
    op0: u8,
    op1: u8,
}

impl Instruction {
    // Instruction constructor, a.k.a. disassembler. Decodes an encoded instruction
    pub fn disassemble(insn: u16) -> Result<Instruction, Error> {
        // transforme insn en un u8 que l'on transforme en OpCode avec la fonction from_u8 de la structure
        let opcode = OpCode::from_u8((insn >> 8) as u8)?;

        // opération binaire pour trouver les 2 opérandes op0 et op1 (& etant la différence)
        // 0xf0 = 240 (1111 0000) et oxf = 15 (0000 1111) (240 + 15 = 255 -> valeur max sur 8 bits)
        // on fait la différence binaire entre 240 et la valeur en question (on obtient les 4 bits de gauche) pour op0
        // on fait la meme chose pour les 4 bits a droite pour op1
        // on fait un decalage de 4 bits vers la droite pour que les 4 bits les plus à gauche se retrouve à droite 
        // et respecte les règles du do-core "4 bits pour l'op0"
        
        let op0: u8 = ((insn & 0xf0) >> 4) as u8;
        let op1: u8 = (insn & 0xf) as u8;

        // on verifie que op0 et op1 ne dépassent pas la taille max (définite dans lib.rs)
        // si c'est le cas ca renvoie une erreur sinon c'est bon
        if op0 > MAX_REGISTER_INDEX {
            return Err(Error::Op0OutOfRange);
        }

        if op1 > MAX_REGISTER_INDEX {
            return Err(Error::Op1OutOfRange);
        }

        return Ok(Instruction {opcode, op0, op1});

    }
    /// Returns this instruction's opcode as an owned [OpCode], and not as a reference !
    pub fn opcode(&self) -> OpCode {
        self.opcode.clone()
    }

    pub fn op0(&self) -> u8 {
        self.op0
    }

    pub fn op1(&self) -> u8 {
        self.op1
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::Error;


    #[test]
    fn test_instruction_disassemble_add_r0_r1() -> Result<(), Error> {
        let insn_bytes: u16 = 0x201;
        let insn = Instruction::disassemble(insn_bytes)?;

        assert_eq!(insn.opcode, OpCode::ADD);
        assert_eq!(insn.op0, 0);
        assert_eq!(insn.op1, 1);

        Ok(())
    }

    #[test]
    fn test_instruction_disassemble_add_r9_r1() -> Result<(), Error> {
        let insn_bytes: u16 = 0x291;
        assert!(Instruction::disassemble(insn_bytes).is_err());

        Ok(())
    }

    #[test]
    fn test_instruction_disassemble_add_r0_r10() -> Result<(), Error> {
        let insn_bytes: u16 = 0x20a;
        assert!(Instruction::disassemble(insn_bytes).is_err());

        Ok(())
    }

    #[test]
    fn test_instruction_disassemble_add_r7_r2() -> Result<(), Error> {
        let insn_bytes: u16 = 0x272;
        let insn = Instruction::disassemble(insn_bytes)?;

        assert_eq!(insn.opcode, OpCode::ADD);
        assert_eq!(insn.op0, 7);
        assert_eq!(insn.op1, 2);

        Ok(())
    }

    #[test]
    fn test_instruction_disassemble_ld_r0_r1() -> Result<(), Error> {
        let insn_bytes: u16 = 0x01;
        let insn = Instruction::disassemble(insn_bytes)?;

        assert_eq!(insn.opcode, OpCode::LD);
        assert_eq!(insn.op0, 0);
        assert_eq!(insn.op1, 1);

        Ok(())
    }

    #[test]
    fn test_instruction_disassemble_xor_r2_r3() -> Result<(), Error> {
        let insn_bytes: u16 = 0x323;
        let insn = Instruction::disassemble(insn_bytes)?;

        assert_eq!(insn.opcode, OpCode::XOR);
        assert_eq!(insn.op0, 2);
        assert_eq!(insn.op1, 3);

        Ok(())
    }

    #[test]
    fn test_instruction_disassemble_st_r5_r0() -> Result<(), Error> {
        let insn_bytes: u16 = 0x150;
        let insn = Instruction::disassemble(insn_bytes)?;

        assert_eq!(insn.opcode, OpCode::ST);
        assert_eq!(insn.op0, 5);
        assert_eq!(insn.op1, 0);

        Ok(())
    }
}
